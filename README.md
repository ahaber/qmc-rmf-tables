# QMC-RMF Tables

We are providing the fully tabulated relativistic mean field models QMC-RMF1/2/3/4 developed in https://arxiv.org/abs/2205.10283 as "hdf5" files.
For more information see the accompanying arXiv document https://arxiv.org/abs/2304.07836.

All quantities are tabulated as a function of the three independent variables temperature "T" (in $\text{MeV}$), baryon density "nB" (in $\text{MeV}^3$) and proton fraction "xp".

## Independent Variables

We provide three 1-D tables:

- "Tpoints" includes all temperature values in $\text{MeV}$
- "nbpoints" includes all density values in $\text{MeV}^3$
- "xppoints" includes all proton fraction values (dimensionless, ratio of proton number and total baryon number. Note that charge neutrality is enforced and we only included neutrons, protons and electrons, the proton number is therefore equal to the electron number and the total charge fraction)

## Tabulated Quantities

The following 3-D tables are indexed by a proton fraction index xp_ind, a temperature index T_ind, and a density index nB_ind (in that order). We provide:

### The chemical potentials:

- "mun": neutron chemical potential $\mu_n$ in MeV
- "mup": proton chemical potential $\mu_p$ in MeV
- "mue": electron chemical potential $\mu_e$ in MeV
- "muz": "Z"-chemical potential $\mu_Z$ in MeV (phase transitions are computed at constant $\mu_Z$)

### The thermodynamic quantities:

- "pressure": total pressure in $\text{MeV}^4$
- "energydensity": total energy density in $\text{MeV}^4$
- "entropydensity": total entropy density (NOT entropy per baryon) in $\text{MeV}^3$

### Additional microscopic information:

- "NeutronDiracMass": the Dirac effective mass of the neutron $m_D$ divided by the nucleon vacuum mass $m_N=939$ $\text{MeV}$ (dimensionless)
- "ProtonDiracMass": the Dirac effective mass of the proton $m_D$ divided by the nucleon vacuum mass $m_N=939$ $\text{MeV}$ (dimensionless)
- "NeutronVectorShift": the energy shift of the neutron due to its coupling to the vector mesons $U_i = -g_{\omega}\langle\omega_0\rangle-g_{\rho}I_{3i}\langle\rho_{03}\rangle$
- "ProtonVectorShift": the energy shift of the proton due to its coupling to the vector mesons $U_i = -g_{\omega}\langle\omega_0\rangle-g_{\rho}I_{3i}\langle\rho_{03}\rangle$
- "PhaseIndex": The phase index tells you the phase for any given data point: "1" means that matter is in the low-density (largely inhomogeneous) phase modeled by HS(IUF), "3" is the homogeneous phase described by QMC-RMF and "2" refers to a mixed phase between phase 1 and 2. For the mixed phase we rely on linear interpolation for all quantities.
